<h1> Proton Synchrotron Booster Optics Repository </h1>

This website contains the official optics models for the CERN Proton Synchrotron Booster. For each scenario listed below 
(and the various configurations belonging to each scenario), MAD-X input scripts, Twiss tables and
optics plots are available and can be browsed.  

<h2> Operational optics scenarios</h2>


- [LHC](scenarios/LHC/index.md) - Proton beams produced for the LHC physics programme.


<h2> Data structure </h2>

The data for the different operational scenarios quoted above are organized in the following way:

<ul>
<li><b>Optics scenario</b>: each scenario corresponds to a certain operational cycle (i.e. beam and/or user).</li>

<li><b>Configuration</b>: the state of the machine for a specific instant along the cycle (e.g. injection,
flat bottom, ...).</li>

</ul>

For each configuration the values of several parameters (energy, transverse tunes, optical functions at the various BI monitors) 
as well as Twiss tables and plots of the optics functions are provided.
