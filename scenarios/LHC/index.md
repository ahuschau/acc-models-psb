<h1> LHC optics</h1>

<h2> Description </h2>

<p> Operational scenario for the proton beams produced for the LHC physics programme.  </p>

<h2> Data table </h2>

The following table contains the most important parameters for each configuration. In addition, the optics functions at the profile monitors are shown.
<p>

<table border="0">
  <tr>
    <th id="CELL1" colspan="10" align=center> </th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWS.2L1.V_ROT</b></th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWS.2L1.V_ROT</b></th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWS.2L1.H_ROT</b></th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWS.2L1.H_ROT</b></th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWSV11L1</b></th>
    <th id="CELL2" colspan="4" align = center> <b>BR1.BWSV11L1</b></th>
    </tr>
  <tr>   
    <th> <b>Configuration</b> </th>
    <th align=center> <b>E<sub>kin</sub> [GeV]</b> </th>
    <th align=center> <b>E<sub>tot</sub> [GeV]</b> </th>
    <th align=center> <b>&gamma;<sub>rel</sub></b> </th>
    <th align=center> <b>&beta;<sub>rel</sub></b> </th>
    <th align=center> <b>p [GeV/c]</b> </th>
    <th align=center> <b>Q<sub>x</sub></b> </th>
    <th align=center> <b>Q<sub>y</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>x</sub></b> </th>
    <th align=center> <b>Q&prime;<sub>y</sub></b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    <th align=center> <b>s [m]</b> </th>
    <th align=center> <b>&beta;<sub>x</sub> [m]</b> </th>
    <th align=center> <b>&beta;<sub>y</sub> [m]</b> </th>
    <th align=center> <b>D<sub>x</sub> [m]</b> </th>
    </tr>
  <tr>
    <td> <a href="1_flat_bottom/index.html">flat bottom </a></td>
    <td align="center">0.16</td>
    <td align="center">1.1</td>
    <td align="center">1.17</td>
    <td align="center">0.52</td>
    <td align="center">0.57</td>
    <td align="center">4.28</td>
    <td align="center">4.45</td>
    <td align="center">-3.46</td>
    <td align="center">-7.28</td>
    
    <td align="center">10.736</td>
    <td align="center">5.538</td>
    <td align="center">3.895</td>
    <td align="center">-1.391</td>
    
    <td align="center">10.736</td>
    <td align="center">5.538</td>
    <td align="center">3.895</td>
    <td align="center">-1.391</td>
    
    <td align="center">10.834</td>
    <td align="center">5.525</td>
    <td align="center">3.876</td>
    <td align="center">-1.391</td>
    
    <td align="center">10.834</td>
    <td align="center">5.525</td>
    <td align="center">3.876</td>
    <td align="center">-1.391</td>
    
    <td align="center">100.188</td>
    <td align="center">5.592</td>
    <td align="center">3.974</td>
    <td align="center">-1.391</td>
    
    <td align="center">100.188</td>
    <td align="center">5.592</td>
    <td align="center">3.974</td>
    <td align="center">-1.391</td>
    <tr>
    <td> <a href="2_flat_top/index.html">flat top </a></td>
    <td align="center">1.4</td>
    <td align="center">2.34</td>
    <td align="center">2.49</td>
    <td align="center">0.92</td>
    <td align="center">2.14</td>
    <td align="center">4.172</td>
    <td align="center">4.23</td>
    <td align="center">-3.34</td>
    <td align="center">-6.69</td>
    
    <td align="center">10.736</td>
    <td align="center">5.712</td>
    <td align="center">4.252</td>
    <td align="center">-1.466</td>
    
    <td align="center">10.736</td>
    <td align="center">5.712</td>
    <td align="center">4.252</td>
    <td align="center">-1.466</td>
    
    <td align="center">10.834</td>
    <td align="center">5.699</td>
    <td align="center">4.235</td>
    <td align="center">-1.466</td>
    
    <td align="center">10.834</td>
    <td align="center">5.699</td>
    <td align="center">4.235</td>
    <td align="center">-1.466</td>
    
    <td align="center">100.188</td>
    <td align="center">5.764</td>
    <td align="center">4.325</td>
    <td align="center">-1.466</td>
    
    <td align="center">100.188</td>
    <td align="center">5.764</td>
    <td align="center">4.325</td>
    <td align="center">-1.466</td>
    </tr>
 
</table>

<p> Symbols:
<ul>
  <li><strong>E<sub>kin</sub></strong>: Kinetic energy of the beam in GeV. </li>
  <li><strong>E<sub>tot</sub></strong>: Total energy of the beam in GeV. </li>
  <li><strong>&gamma;<sub>rel</sub></strong>: Relativistic &gamma; of the beam. </li>
  <li><strong>&beta;<sub>rel</sub></strong>: Relativistic &beta; of the beam. </li>
  <li><strong>p</strong>: Momentum of the beam in GeV/c. </li>
  <li><strong>s</strong>: Longitudinal position of the device in m. </li>
  <li><strong>&beta;<sub>x,y</sub></strong>: &beta;-functions at the specified beam instrumentation. </li>
  <li><strong>D<sub>x</sub></strong>: Horizontal dispersion function at the specified beam instrumentation. </li>
</ul>

</p>